package com.mindvalley.mindvalley_aakashdeepak_android_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.mindvalley.mindvalley_aakashdeepak_android_test.adapter.PinGridAdapter;
import com.mindvalley.mindvalley_aakashdeepak_android_test.asynctask.ServerAsyncTask;
import com.mindvalley.mindvalley_aakashdeepak_android_test.model.Image;
import com.mindvalley.mindvalley_aakashdeepak_android_test.model.Pin;
import com.mindvalley.mindvalley_aakashdeepak_android_test.model.User;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.ConnectionUtil;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.ImagesCache;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.MyApplication;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.RecyclerClickListener;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.SharedPrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private String TAG = "MainActivity";
    private ArrayList<Pin> mDataset;
    private String keyArrayList = "pinArrayList";
    private RecyclerView.Adapter mAdapter;
    private GridLayoutManager lLayout;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    ImagesCache cache = ImagesCache.getInstance();
    SharedPrefUtil shared;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        shared = new SharedPrefUtil(MyApplication.getContext());
        cache.initializeCache();
        mRecyclerView = (RecyclerView) findViewById(R.id.pin_recyclerview);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        lLayout = new GridLayoutManager(MainActivity.this, 2);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(lLayout);

        mDataset = new ArrayList<>();

        //check cache for data
        try {
            if (shared.getJsonArray(keyArrayList) != null) {
                getDataFromCache();
            } else {
                mSwipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(true);
                    }
                });
                makeRequestOnServer();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                makeRequestOnServer();
            }
        });

        mRecyclerView.addOnItemTouchListener(new RecyclerClickListener(MainActivity.this, new RecyclerClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                        intent.putExtra(DetailActivity.Name, mDataset.get(position).getUser().getName());
                        intent.putExtra(DetailActivity.PinImage, mDataset.get(position).getImage().getImage_url());
                        intent.putExtra(DetailActivity.UserImage, mDataset.get(position).getUser().getProfile_image());
                        intent.putExtra(DetailActivity.Like, mDataset.get(position).getLikes());
                        //startActivity(intent);
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                // the context of the activity
                                MainActivity.this,
                                // For each shared element, add to this method a new Pair item,
                                // which contains the reference of the view we are transitioning *from*,
                                // and the value of the transitionName attribute
                                new Pair<View, String>(view.findViewById(R.id.pin_image),
                                        getString(R.string.transition_name_circle)),
                                new Pair<View, String>(view.findViewById(R.id.user_name),
                                        getString(R.string.transition_name_name)),
                                new Pair<View, String>(view.findViewById(R.id.user_image),
                                        getString(R.string.transition_name_name))
                        );

                        ActivityCompat.startActivity(MainActivity.this, intent, options.toBundle());
                    }
                })
        );
    }


    //Request on server to retrieve data
    public void makeRequestOnServer() {
        ConnectionUtil connectionUtil = new ConnectionUtil(MyApplication.getContext());
        if (connectionUtil.isNetworkConnected()) {
            HashMap<String, String> params = new HashMap<>();
            ServerAsyncTask asyncTask = new ServerAsyncTask(new ServerAsyncTask.AsyncResponse() {
                @Override
                public void processFinish(String output) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    if (output != null) {
                        if (output.equals("-1")) {
                            Log.d(TAG, "Exception");
                        } else {
                            try {
                                JSONArray jsonArray = new JSONArray(output.toString());
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Pin pin = new Pin();
                                    pin.setId(jsonObject.getString("id"));
                                    pin.setLikes(jsonObject.getString("likes") + " Likes");
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                                    User user = new User(jsonObject1.optString("id"), jsonObject1.getString("username"), jsonObject1.getString("name"), jsonObject1.getJSONObject("profile_image").getString("medium"));
                                    Image image = new Image(jsonObject.getJSONObject("urls").getString("small"), jsonObject.getJSONObject("urls").getString("small"));
                                    pin.setImage(image);
                                    pin.setUser(user);
                                    mDataset.add(pin);
                                }
                                //Log.d(TAG, mDataset.toString());
                                mAdapter = new PinGridAdapter(MainActivity.this, mDataset);
                                mRecyclerView.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                                shared.removeAndAddJsonArray(keyArrayList, jsonArray);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d(TAG, output.toString());
                    }
                }
            }, "", MainActivity.this, TAG, 2);
            asyncTask.execute(params);
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }


    //If data available in cache, get it from there
    public void getDataFromCache() throws JSONException {
        JSONArray jsonArray = shared.getJsonArray(keyArrayList);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Pin pin = new Pin();
            pin.setId(jsonObject.getString("id"));
            pin.setLikes(jsonObject.getString("likes") + " Likes");
            JSONObject jsonObject1 = jsonObject.getJSONObject("user");
            User user = new User(jsonObject1.optString("id"), jsonObject1.getString("username"), jsonObject1.getString("name"), jsonObject1.getJSONObject("profile_image").getString("medium"));
            Image image = new Image(jsonObject.getJSONObject("urls").getString("small"), jsonObject.getJSONObject("urls").getString("small"));
            pin.setImage(image);
            pin.setUser(user);
            mDataset.add(pin);
        }
        //Log.d(TAG, mDataset.toString());
        mAdapter = new PinGridAdapter(MainActivity.this, mDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}
