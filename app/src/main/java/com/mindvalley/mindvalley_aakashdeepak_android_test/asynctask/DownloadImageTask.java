package com.mindvalley.mindvalley_aakashdeepak_android_test.asynctask;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.mindvalley.mindvalley_aakashdeepak_android_test.adapter.PinGridAdapter;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.ImagesCache;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aakas on 10-07-2016.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private int inSampleSize = 0;

    private String imageUrl;

    private PinGridAdapter adapter;

    private ImagesCache cache;

    private int desiredWidth, desiredHeight;

    private Bitmap image = null;

    private ImageView ivImageView;

    private boolean running = true;

    public DownloadImageTask(PinGridAdapter adapter, int desiredWidth, int desiredHeight) {
        this.adapter = adapter;

        this.cache = ImagesCache.getInstance();

        this.desiredWidth = desiredWidth;

        this.desiredHeight = desiredHeight;
    }

    public DownloadImageTask(ImagesCache cache, ImageView ivImageView, int desireWidth, int desireHeight) {
        this.cache = cache;

        this.ivImageView = ivImageView;

        this.desiredHeight = desireHeight;

        this.desiredWidth = desireWidth;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        imageUrl = params[0];
        while (running) {
            if (isCancelled()) {
                break;
            }else {
                try{
                    return getImage(imageUrl);
                }catch(Exception e) {
                    break;
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        if (result != null) {
            cache.addImageToWarehouse(imageUrl, result);

            if (ivImageView != null) {
                ivImageView.setImageBitmap(result);
            } else {

            }

            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    /*@Override
    protected void onCancelled() {
        super.onCancelled();
        running = false;
    }*/

    private Bitmap getImage(String imageUrl) throws Exception {
        if (cache.getImageFromWarehouse(imageUrl) == null) {
            BitmapFactory.Options options = new BitmapFactory.Options();

            options.inJustDecodeBounds = true;

            options.inSampleSize = inSampleSize;

            try {
                URL url = new URL(imageUrl);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                InputStream stream = connection.getInputStream();

                image = BitmapFactory.decodeStream(stream, null, options);

                int imageWidth = options.outWidth;

                int imageHeight = options.outHeight;

                if (imageWidth > desiredWidth || imageHeight > desiredHeight) {
                    System.out.println("imageWidth:" + imageWidth + ", imageHeight:" + imageHeight);

                    inSampleSize = inSampleSize + 2;

                    getImage(imageUrl);
                } else {
                    options.inJustDecodeBounds = false;

                    connection = (HttpURLConnection) url.openConnection();

                    stream = connection.getInputStream();

                    image = BitmapFactory.decodeStream(stream, null, options);

                    return image;
                }
            } catch (Exception e) {
                Log.e("getImage", e.toString());
                throw new Exception();
            }
        }

        return image;
    }

}