package com.mindvalley.mindvalley_aakashdeepak_android_test.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.Constants;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.Logging;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.ServerUtils;

import java.util.HashMap;

/**
 * Created by aakas on 7/9/2016.
 */
public class ServerAsyncTask extends AsyncTask<HashMap<String, String>, Void, String> {
    private String url;
    private String response = "0";
    private String TAGNAME;
    int put = 0;
    private long time;
    private ServerUtils serverUtils;

    public interface AsyncResponse {
        void processFinish(String output);
    }

    public AsyncResponse delegate = null;

    // ProgressDialog pDialog;
    public ServerAsyncTask(AsyncResponse delegate, String urls, Context context, String Tag) {
        this.delegate = delegate;
        url = Constants.API_URL + urls;
        serverUtils = new ServerUtils(context);
        TAGNAME = Tag;
    }

    public ServerAsyncTask(AsyncResponse delegate, String urls, Context context, String Tag, int put) {
        this.delegate = delegate;
        url = Constants.API_URL + urls;
        serverUtils = new ServerUtils(context);
        TAGNAME = Tag;
        this.put = put;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(HashMap<String, String>... params) {
        Log.d(TAGNAME, " do in background..");
        try {
            if (put == 0) {
                response = serverUtils.performPostCall(url, params[0]);
            } else if (put == 1) {
                response = serverUtils.performPutCall(url, params[0]);
            } else if (put == 2) {
                response = serverUtils.performGetCall(url);
            } else {
                response = serverUtils.performDeleteCall(url, params[0]);
            }
            return response;
        } catch (Exception e) {
            time = System.currentTimeMillis() - time;
            Logging.showLog("ServerAsyncTask", "Exception" + e.getMessage());
            response = "-1";
            return response;
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (delegate != null) {
            delegate.processFinish(response.toString());
            Log.d(TAGNAME, "Cancelled " + response.toString());
        }
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        time = System.currentTimeMillis() - time;
        delegate.processFinish(response.toString());
        Log.d(TAGNAME, response.toString());
    }
}
