package com.mindvalley.mindvalley_aakashdeepak_android_test.model;

/**
 * Created by aakas on 09-07-2016.
 */
public class Pin {
    private String id;
    private String created_at;
    private String height;
    private String width;
    private String color;
    private String likes;
    private User user;
    private Image image;

    public Pin() {
    }

    public Pin(String id, String created_at, String height, String width, String color, String likes, User user, Image image) {
        this.id = id;
        this.created_at = created_at;
        this.height = height;
        this.width = width;
        this.color = color;
        this.likes = likes;
        this.user = user;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

}
