package com.mindvalley.mindvalley_aakashdeepak_android_test.model;

/**
 * Created by aakas on 09-07-2016.
 */
public class User {
    private String id;
    private String username;
    private String name;
    private String profile_image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public User() {
    }

    public User(String id, String username, String name, String profile_image) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.profile_image = profile_image;
    }


}
