package com.mindvalley.mindvalley_aakashdeepak_android_test.model;

/**
 * Created by aakas on 09-07-2016.
 */
public class Image {
    private String image_url;
    private String image_download_link;

    public Image() {
    }

    public Image(String image_url, String image_download_link) {
        this.image_url = image_url;
        this.image_download_link = image_download_link;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getImage_download_link() {
        return image_download_link;
    }

    public void setImage_download_link(String image_download_link) {
        this.image_download_link = image_download_link;
    }
}
