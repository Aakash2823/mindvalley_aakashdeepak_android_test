package com.mindvalley.mindvalley_aakashdeepak_android_test.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mindvalley.mindvalley_aakashdeepak_android_test.R;
import com.mindvalley.mindvalley_aakashdeepak_android_test.asynctask.DownloadImageTask;
import com.mindvalley.mindvalley_aakashdeepak_android_test.model.Image;
import com.mindvalley.mindvalley_aakashdeepak_android_test.model.Pin;
import com.mindvalley.mindvalley_aakashdeepak_android_test.model.User;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.Constants;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.ImagesCache;

import java.util.ArrayList;

/**
 * Created by aakas on 09-07-2016.
 */
public class PinGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Activity context;
    private ArrayList<Pin> mDataset;
    ImagesCache cache = ImagesCache.getInstance();
    private String TAG = "PinGridAdapter";
    private DownloadImageTask imgTask1;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView user_image;
        AppCompatImageView pin_image;
        TextView username, likes;
        FrameLayout progressLayout;
        ProgressBar progressBar;
        LinearLayout mainLayout;

        public ViewHolder(View v) {
            super(v);
            user_image = (AppCompatImageView) v.findViewById(R.id.user_image);
            pin_image = (AppCompatImageView) v.findViewById(R.id.pin_image);
            username = (TextView) v.findViewById(R.id.user_name);
            likes = (TextView) v.findViewById(R.id.likes);
            progressLayout = (FrameLayout) v.findViewById(R.id.progressLayout);
            progressBar = (ProgressBar) v.findViewById(R.id.progress);
            mainLayout = (LinearLayout) v.findViewById(R.id.mainLayout);
//            mImageView=(ImageView) v.findViewById(R.id.imagess);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PinGridAdapter(Activity activity, ArrayList<Pin> myDataset) {
        context = activity;
        mDataset = new ArrayList<>();
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pin_grid_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        //  ...
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final ViewHolder viewHolder = (ViewHolder) holder;
        Pin pin = mDataset.get(position);
        Image image = pin.getImage();
        User user = pin.getUser();
        String url = image.getImage_url();
        String user_image_url = user.getProfile_image();
        Bitmap bm1 = cache.getImageFromWarehouse(user_image_url);
        if (bm1 != null) {
            viewHolder.user_image.setImageBitmap(Constants.getCircleBitmap(bm1));
        } else {
            //Log.d(TAG , "User Download started");
            viewHolder.user_image.setImageResource(R.drawable.image_placeholder);

            DownloadImageTask imgTask = new DownloadImageTask(PinGridAdapter.this, 100, 100);//Since you are using it from `Adapter` call first Constructor.

            imgTask.execute(user_image_url);
        }

        Bitmap bm = cache.getImageFromWarehouse(url);
        if (bm != null) {
            viewHolder.pin_image.setImageBitmap(bm);
            viewHolder.progressLayout.setVisibility(View.GONE);
        } else {
            //Log.d(TAG , "Image Download started");
            viewHolder.progressLayout.setVisibility(View.VISIBLE);
            viewHolder.pin_image.setImageResource(R.drawable.image_placeholder);

            imgTask1 = new DownloadImageTask(PinGridAdapter.this, 300, 300);//Since you are using it from `Adapter` call first Constructor.

            imgTask1.execute(url);
        }

        viewHolder.username.setText(user.getName());
        viewHolder.likes.setText(pin.getLikes());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


}