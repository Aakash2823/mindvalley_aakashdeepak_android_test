package com.mindvalley.mindvalley_aakashdeepak_android_test;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mindvalley.mindvalley_aakashdeepak_android_test.asynctask.DownloadImageTask;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.Constants;
import com.mindvalley.mindvalley_aakashdeepak_android_test.utils.ImagesCache;

public class DetailActivity extends AppCompatActivity {
    public final static String Name = "Name";
    public final static String PinImage = "PinImage";
    public final static String UserImage = "UserImage";
    public final static String Like = "Like";
    private AppCompatImageView user_image;
    private AppCompatImageView pin_image;
    private TextView username, likes;
    private FrameLayout progressLayout;
    ImagesCache cache = ImagesCache.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
       // cache.initializeCache();
        user_image = (AppCompatImageView) findViewById(R.id.user_image);
        pin_image = (AppCompatImageView) findViewById(R.id.pin_image);
        username = (TextView) findViewById(R.id.user_name);
        likes = (TextView) findViewById(R.id.likes);
        progressLayout = (FrameLayout) findViewById(R.id.progressLayout);

        String url = getIntent().getStringExtra(PinImage);
        String user_image_url = getIntent().getStringExtra(UserImage);

        Bitmap bm1 = cache.getImageFromWarehouse(user_image_url);
        if (bm1 != null) {
            user_image.setImageBitmap(Constants.getCircleBitmap(bm1));
        } else {
            //Log.d(TAG , "User Download started");
            user_image.setImageResource(R.drawable.image_placeholder);

            DownloadImageTask imgTask = new DownloadImageTask(cache, user_image, 100, 100);

            imgTask.execute(user_image_url);
        }

        Bitmap bm = cache.getImageFromWarehouse(url);
        if (bm != null) {
            pin_image.setImageBitmap(bm);
            progressLayout.setVisibility(View.GONE);
        } else {
            //Log.d(TAG , "Image Download started");
            progressLayout.setVisibility(View.GONE);
            pin_image.setImageResource(R.drawable.image_placeholder);

            DownloadImageTask imgTask1 = new DownloadImageTask(cache, pin_image, 300, 300);

            imgTask1.execute(url);
        }

        username.setText(getIntent().getStringExtra(Name));
        likes.setText(getIntent().getStringExtra(Like));
    }


}
