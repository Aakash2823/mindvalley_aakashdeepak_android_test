package com.mindvalley.mindvalley_aakashdeepak_android_test.utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by aakas on 09-07-2016.
 */
public class MyApplication extends Application {
    private static Application instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }
}
