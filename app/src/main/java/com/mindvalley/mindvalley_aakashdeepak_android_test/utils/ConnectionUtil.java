package com.mindvalley.mindvalley_aakashdeepak_android_test.utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by aakas on 1/20/2016.
 */
public class ConnectionUtil {
    Context context;

    public ConnectionUtil(Context context) {
        this.context = context;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) MyApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
