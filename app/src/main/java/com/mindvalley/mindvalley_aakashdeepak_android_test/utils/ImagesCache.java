package com.mindvalley.mindvalley_aakashdeepak_android_test.utils;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

/**
 * Created by aakas on 10-07-2016.
 */
public class ImagesCache {
    private LruCache<String, Bitmap> imagesWarehouse;
    private LruCache<String, JSONArray> pinsWarehouse;

    private static ImagesCache cache;

    public static ImagesCache getInstance() {
        if (cache == null) {
            cache = new ImagesCache();
        }
        return cache;
    }

    //intitaloze cache once
    public void initializeCache() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        final int cacheSize = maxMemory / 8;

        System.out.println("cache size = " + cacheSize);

        imagesWarehouse = new LruCache<String, Bitmap>(cacheSize) {
            protected int sizeOf(String key, Bitmap value) {
                // The cache size will be measured in kilobytes rather than number of items.

                int bitmapByteCount = value.getRowBytes() * value.getHeight();

                return bitmapByteCount / 1024;
            }
        };

        pinsWarehouse = new LruCache<String, JSONArray>(cacheSize) {
            @Override
            protected int sizeOf(String key, JSONArray value) {
                return super.sizeOf(key, value);
            }
        };
    }

    //add Object to lrucache
    public void addImageToWarehouse(String key, Bitmap value) {
        if (imagesWarehouse != null && imagesWarehouse.get(key) == null) {
            imagesWarehouse.put(key, value);
        }
    }

    //get Object from lrucache
    public Bitmap getImageFromWarehouse(String key) {
        if (key != null) {
            return imagesWarehouse.get(key);
        } else {
            return null;
        }
    }

    public void addJsonArrayToWarehouse(String key, JSONArray value) {
        if (pinsWarehouse != null && pinsWarehouse.get(key) == null) {
            pinsWarehouse.put(key, value);
        }
    }

    public JSONArray getJsonArrayFromWareHouse(String key) {
        if (key != null) {
            return pinsWarehouse.get(key);
        } else {
            return null;
        }
    }

    public void removeImageFromWarehouse(String key) {
        imagesWarehouse.remove(key);
    }

    public void clearImageCache() {
        if (imagesWarehouse != null) {
            imagesWarehouse.evictAll();
        }
    }

    public void removeArrayListFromWarehouse(String key) {
        pinsWarehouse.remove(key);
    }

    public void clearArrayListCache() {
        if (pinsWarehouse != null) {
            pinsWarehouse.evictAll();
        }
    }

    public static byte[] sizeOf(Object obj) throws java.io.IOException {
        ByteArrayOutputStream byteObject = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteObject);
        objectOutputStream.writeObject(obj);
        objectOutputStream.flush();
        objectOutputStream.close();
        byteObject.close();

        return byteObject.toByteArray();
    }
}
