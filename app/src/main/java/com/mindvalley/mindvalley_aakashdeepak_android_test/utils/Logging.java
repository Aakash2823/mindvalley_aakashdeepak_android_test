package com.mindvalley.mindvalley_aakashdeepak_android_test.utils;

import android.util.Log;

/**
 * Created by aakas on 7/9/2016.
 */
public class Logging {


    public Logging() {
    }

    public static void showLog(String TAG, String message) {
        Log.d(TAG, message);
    }

}
